<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<logic:notPresent name="userListBean" scope="session">
	<logic:redirect forward="error" />
</logic:notPresent>
<html:html>
<head>
<title><bean:message key="users.title" /></title>
</head>
<body>
	<div>

		<h3>
			<font color="blue"><bean:message key="users.title" /></font>
		</h3>
		<table width="80%" border="1">
			<tr>
				<th width="10%"><bean:message key="prompt.idUsuario" /></th>
				<th width="50%"><bean:message key="prompt.nome" /></th>
				<th width="20%"><bean:message key="prompt.login" /></th>
				<th width="10%"><bean:message key="prompt.ativo" /></th>
				<th width="10%"></th>
			</tr>
			<%-- loop que percorre a Collection de usuarios --%>
			<logic:iterate name="userListBean" id="user">
				<tr>
					<td align="center"><bean:write name="user"
							property="idUsuario" /></td>
					<td><html:link page="/editUser.do" paramId="idUsuario"
							paramName="user" paramProperty="idUsuario">
							<bean:write name="user" property="nome" />
						</html:link></td>
					<td><bean:write name="user" property="login" /></td>
					<td><bean:write name="user" property="descricaoStatus" /></td>
					<td><html:link page="/deleteUser.do" paramId="idUsuario"
							paramName="user" paramProperty="idUsuario">
							<bean:message key="prompt.excluir" />
						</html:link></td>
				</tr>
			</logic:iterate>
		</table>
		<br />
		<html:link page="/insertUser.do">incluir</html:link>
		<html:link page="/welcome.do">P�gina Inicial</html:link>
	</div>
</body>
</html:html>

