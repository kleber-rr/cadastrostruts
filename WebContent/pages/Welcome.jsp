<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<html:html>
<head>
<title><bean:message key="welcome.title" /></title>
<html:base/>
</head>
<body bgcolor="white">
	<div>

		<h1>
			<font color="blue"><bean:message key="welcome.title" /></font>
		</h1>
		<logic:notPresent name="org.apache.struts.action.MESSAGE"
			scope="application">
			<font color="red"> ERROR: Application resources not loaded --
				check servlet container logs for error messages. </font>
		</logic:notPresent>
		<h3>
			<bean:message key="welcome.heading" />
		</h3>
		<p>
			<bean:message key="welcome.message" />
		</p>
		<html:link page="/listUsers.do">Cadastro de Usu�rios</html:link>

		<p>
			<font color="darkblue"> Autor: <html:link
					href="mailto:wbsouza@yahoo.com.br">Welington
B.Souza</html:link> <br>01/07/2003
			</font>
		</p>

	</div>
</body>
</html:html>