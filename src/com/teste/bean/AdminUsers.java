package com.teste.bean;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import com.teste.data.UserData;

public class AdminUsers {
	protected static DataSource dataSource;

	public AdminUsers() throws Exception {
		if (dataSource == null) {
			try {
//				InitialContext ic = new InitialContext();
				// se for tomcat
//				dataSource = (DataSource) ic.lookup("java:jdbc/struts");
				Context initContext = new InitialContext();
				Context envContext = (Context)initContext.lookup( "java:/comp/env" );
				dataSource = (DataSource)envContext.lookup( "jdbc/PostgreSQL" );
//				Connection conn = ds.getConnection();
			} catch (NamingException ex) {
				System.out.println(ex.getMessage());
				throw ex;
			}
		}
	}

	protected Connection getConnection() throws SQLException {
		Connection conn = null;
		try {
			conn = dataSource.getConnection();
		} catch (SQLException e) {
			throw e;
		}
		return conn;
	}

	protected void closeConnection(Connection conn, PreparedStatement stmt, ResultSet rs) {
		if (rs != null) {
			try {
				rs.close();
			} catch (SQLException e) {
			}
		}
		if (stmt != null) {
			try {
				stmt.close();
			} catch (SQLException e) {
			}
		}
		if (conn != null) {
			try {
				conn.close();
			} catch (SQLException e) {
			}
		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public LinkedList getUserList() throws SQLException {
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		LinkedList users = new LinkedList();
		try {
			conn = getConnection();
			stmt = conn.prepareStatement("select * from usuario");
			rs = stmt.executeQuery();
			while (rs.next()) {
				UserData user = new UserData();
				user.setIdUsuario(rs.getInt("id_usuario"));
				user.setNome(rs.getString("nome"));
				user.setLogin(rs.getString("login"));
				user.setSenha(rs.getString("senha"));
				user.setSexo(rs.getString("sexo"));
				user.setAtivo(rs.getBoolean("ativo"));
				user.setFaixaIdade(rs.getInt("faixa_idade"));
				users.add(user);
			}
		} catch (SQLException e) {
			throw e;
		} finally {
			closeConnection(conn, stmt, rs);
		}
		return users;
	}

	public void insertUser(UserData user) throws SQLException {
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			conn = getConnection();
			stmt = conn.prepareStatement("insert into usuario \n"
					+ "(id_usuario, nome, login, senha, sexo, ativo, faixa_idade) \n" + "values (?, ?, ?, ?, ?, ?, ?)");
			stmt.setInt(1, user.getIdUsuario());
			stmt.setString(2, user.getNome());
			stmt.setString(3, user.getLogin());
			stmt.setString(4, user.getSenha());
			stmt.setString(5, user.getSexo());
			stmt.setBoolean(6, user.getAtivo());
			stmt.setInt(7, user.getFaixaIdade());
			stmt.executeUpdate();
		} catch (SQLException e) {
			throw e;
		} finally {
			if (rs != null) {
				rs.close();
			}
			if (stmt != null) {
				stmt.close();
			}
		}
	}

	public void updateUser(UserData user) throws SQLException {
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			conn = getConnection();
			stmt = conn.prepareStatement(
					"update usuario set \n" + "nome = ?, login = ?, senha = ?, sexo = ?, ativo = ?, faixa_idade = ? \n"
							+ "where id_usuario = ?");
			stmt.setString(1, user.getNome());
			stmt.setString(2, user.getLogin());
			stmt.setString(3, user.getSenha());
			stmt.setString(4, user.getSexo());
			stmt.setBoolean(5, user.getAtivo());
			stmt.setInt(6, user.getFaixaIdade());
			stmt.setInt(7, user.getIdUsuario());
			stmt.executeUpdate();
		} catch (SQLException e) {
			throw e;
		} finally {
			if (rs != null) {
				rs.close();
			}
			if (stmt != null) {
				stmt.close();
			}
		}
	}

	public void deleteUser(int idUsuario) throws SQLException {
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			conn = getConnection();
			stmt = conn.prepareStatement("delete from usuario where id_usuario = ?");
			stmt.setInt(1, idUsuario);
			stmt.executeUpdate();
		} catch (SQLException e) {
			throw e;
		} finally {
			if (rs != null) {
				rs.close();
			}
			if (stmt != null) {
				stmt.close();
			}
		}
	}
}