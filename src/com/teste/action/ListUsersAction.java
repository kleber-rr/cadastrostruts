package com.teste.action;

import java.sql.SQLException;
import java.util.LinkedList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.teste.bean.AdminUsers;

public class ListUsersAction extends Action {
	/* Met�do que invoca a camada de neg�cios. */
	@SuppressWarnings("rawtypes")
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		LinkedList users = null;
		ActionErrors errors = new ActionErrors();
		try {
			AdminUsers adminUsers = new AdminUsers();
			users = adminUsers.getUserList();
			HttpSession session = request.getSession();
			session.setAttribute("userListBean", users);
		} catch (SQLException e) {
			errors.add(ActionErrors.GLOBAL_MESSAGE, new ActionMessage("error.user.list"));
			getServlet().log("Erro carregando a lista de usu�rios", e);
		}
		if (!errors.isEmpty()) {
			saveErrors(request, errors);
			return (mapping.findForward("failure"));
		} else {
			return (mapping.findForward("success"));
		}
	}

}