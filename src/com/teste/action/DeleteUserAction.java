package com.teste.action;

import java.util.Iterator;
import java.util.LinkedList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.teste.bean.AdminUsers;
import com.teste.data.UserData;

public class DeleteUserAction extends Action {
	@SuppressWarnings("rawtypes")
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		HttpSession session = request.getSession();
		String idUsuario = request.getParameter("idUsuario");
		ActionErrors errors = new ActionErrors();
		try {
			LinkedList userList = (LinkedList) session.getAttribute("userListBean");
			Iterator iter = userList.iterator();
			while (iter.hasNext()) {
				UserData user = (UserData) iter.next();
				if (user.getIdUsuario() == Integer.parseInt(idUsuario)) {
					AdminUsers adminUsers = new AdminUsers();
					adminUsers.deleteUser(Integer.parseInt(idUsuario));
					userList.remove(user);
					break;
				}
			}
		} catch (Exception e) {
			errors.add(ActionErrors.GLOBAL_MESSAGE, new ActionMessage("error.delete.user"));
			getServlet().log("Erro carregando a lista de usu�rios", e);
		}
		if (!errors.isEmpty()) {
			saveErrors(request, errors);
			return (mapping.findForward("failure"));
		} else {
			return (mapping.findForward("success"));
		}
	}
}