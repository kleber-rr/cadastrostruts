package com.teste.action;

import java.sql.SQLException;
import java.util.LinkedList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.validator.DynaValidatorForm;

import com.teste.bean.AdminUsers;
import com.teste.data.UserData;

public class SaveInsertUserAction extends Action {
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		DynaValidatorForm dynaForm = (DynaValidatorForm) form;
		ActionErrors errors = new ActionErrors();
		String senha1 = (String) dynaForm.get("senha");
		String senha2 = (String) dynaForm.get("confirmacaoSenha");
		// como utilizamos um DynamicForm, precisamos terminar a valida��o aqui.
		if (senha1.equals(senha2)) {
			try {
				HttpSession session = request.getSession();
				// popula o bean do usuario com os dados que vieram do Form
				UserData user = (UserData) session.getAttribute("insertUserBean");
				user.setIdUsuario(Integer.parseInt((String) dynaForm.get("idUsuario")));
				user.setLogin((String) dynaForm.get("login"));
				user.setNome((String) dynaForm.get("nome"));
				user.setFaixaIdade(Integer.parseInt((String) dynaForm.get("faixaIdade")));
				user.setSexo((String) dynaForm.get("sexo"));
				user.setNome((String) dynaForm.get("nome"));
				user.setSenha(senha1);
				boolean ativo = ((String) dynaForm.get("ativo")).equals("on");
				user.setAtivo(ativo);
				AdminUsers adminUsers = new AdminUsers();
				adminUsers.insertUser(user);
				LinkedList userList = (LinkedList) session.getAttribute("userListBean");
				userList.add(user);
				session.removeAttribute("insertUserBean");
			} catch (SQLException e) {
				if (e.getErrorCode() == 1062) {
					errors.add(ActionErrors.GLOBAL_MESSAGE, new ActionMessage("error.idUsuario.duplicateKey"));
				} else {
					errors.add(ActionErrors.GLOBAL_MESSAGE, new ActionMessage("error.insert.user"));
				}
			}
		} else {
			errors.add(ActionErrors.GLOBAL_MESSAGE, new ActionMessage("error.ConfirmacaoSenha"));
		}
		if (!errors.isEmpty()) {
			saveErrors(request, errors);
			return (mapping.findForward("error"));
		} else {
			return (mapping.findForward("success"));
		}
	}

}
