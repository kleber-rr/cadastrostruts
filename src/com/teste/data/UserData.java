package com.teste.data;

public class UserData {
	private int idUsuario;
	private String nome;
	private String login;
	private String senha;
	private String sexo;
	private boolean ativo;
	private int faixaIdade;

	public void setIdUsuario(int idUsuario) {
		this.idUsuario = idUsuario;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public void setSexo(String sexo) {
		this.sexo = sexo;
	}

	public void setAtivo(boolean ativo) {
		this.ativo = ativo;
	}

	public void setFaixaIdade(int faixaIdade) {
		this.faixaIdade = faixaIdade;
	}

	public int getIdUsuario() {
		return idUsuario;
	}

	public String getLogin() {
		return login;
	}

	public String getNome() {
		return nome;
	}

	public String getSenha() {
		return senha;
	}

	public String getSexo() {
		return sexo;
	}

	public boolean getAtivo() {
		return ativo;
	}

	public String getDescricaoStatus() {
		return this.ativo ? "Ativo" : "Inativo";
	}

	public int getFaixaIdade() {
		return faixaIdade;
	}
}
